import styles from '../../styles/aboutFront.module.css'
import GoToDown from '../goToDown/goToDown'
const AboutFront = () => {
    return (
        <>
            <article className={styles.aboutFrontArticle} id="aboutFront">
                <div className={styles.aboutFrontText}>
                    <p className={styles.aboutFrontP}>" Front-end web development, also known as client-side development is the practice of producing HTML, CSS and JavaScript for a website or Web Application so that a user can see and interact with them directly. The challenge associated with front end development is that the tools and techniques used to create the front end of a website change constantly and so the developer needs to constantly be aware of how the field is developing. "</p>
                    <a className={styles.aboutFrontLink} target="blank" href="https://frontendmasters.com/books/front-end-handbook/2018/what-is-a-FD.html">Reference</a>
                </div>
                <div className={styles.aboutFrontImage}>
                    <div className={styles.imgWrapper}>
                        <img className={styles.aboutFrontImg} src={"/frontEnd.jpg"} alt="front-end development" />
                    </div>
                </div>
            </article>
            <GoToDown goto={'#skills'} title={"let's have a look at my skills"} />
        </>
    )
}

export default AboutFront
