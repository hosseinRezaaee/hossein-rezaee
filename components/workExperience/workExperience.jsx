import styles from '../../styles/workExperience.module.css'
import GoToDown from '../goToDown/goToDown'
import Project from './project'

const WorkExperience = () => {
    return (
        <>
            <article className={styles.worksArticle} id="projects">
                <Project color={'#D63A5F'} imgSrc={"/my-website.png"} imgAlt={'hossein rezaee'} bodyColor={'MY_WEBSITE'}/>
                <Project color={'#00A79D'} imgSrc={"/feenab.png"} imgAlt={'feenab.com'} bodyColor={'FEENAB'} link={'https://feenab.com'}/>
                <Project color={'#7DC148'}  imgSrc={"/nouyan.png"} imgAlt={'nouyan-co.com'} bodyColor={'NOUYAN'} link={'https://nouyan-co.com'}/>
            </article>   
            <GoToDown goto={"#contactMe"} title={"contact me through"}/>
        </>
    )
}

export default WorkExperience
