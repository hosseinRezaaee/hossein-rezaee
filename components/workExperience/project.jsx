import styles from '../../styles/project.module.css'
import { useDispatch } from 'react-redux'
import Link from 'next/link'
import store from '../../redux/store'

const Project = ({ color, imgSrc, imgAlt, bodyColor, link }) => {

    const dispath = useDispatch()

    const changeBgColor = (color) => {
        if (color) {
            dispath({ type: color })
        } else {
            dispath({ type: 'doDefault' })

        }
    }
    let retProj;
    if (link) {
        retProj =
            <Link href={link}>
                <div className={styles.project}>
                    <img src={imgSrc} alt={imgAlt} className={styles.projectImg} />
                    <div className={styles.projectBackDrop} style={{ backgroundColor: color }}  onMouseEnter={() => { changeBgColor(bodyColor) }} onMouseLeave={changeBgColor}></div>
                </div>
            </Link>
    } else {
        retProj =
            <div className={styles.project}>
                <img src={imgSrc} alt={imgAlt} className={styles.projectImg} />
                <div className={styles.projectBackDrop} style={{ backgroundColor: color }}  onMouseEnter={() => { changeBgColor(bodyColor) }} onMouseLeave={changeBgColor}></div>
            </div>
    }

    return (
        <>
            {retProj}
        </>
    )
}

export default Project
