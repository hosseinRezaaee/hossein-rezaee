import styles from '../../styles/goToDown.module.css'
const GoToDown = ({goto , title}) => {
    return (
        <div className={styles.goToDown}>
                <a href={goto} className={styles.goToDownLink}>{title}<span className={styles.goToDownArrow}>↓</span></a>
            </div>
    )
}

export default GoToDown
