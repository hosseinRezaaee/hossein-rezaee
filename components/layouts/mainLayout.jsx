import HeaderLayout from "./headerLayout"

const MainLayout = (props) => {
    return (
        <>
            <HeaderLayout/>
            <main className="main">
                {props.children}
            </main>
            <style jsx>{`
                .main {
                    width: 100%;
                    display: flex;
                    flex-flow: column nowrap;
                    padding: 30px 0;
                    align-items: center;
                    justify-content: center;
                }    
            `}</style>
        </>
    )
}

export default MainLayout
