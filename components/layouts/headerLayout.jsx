import Link from 'next/link'
import styles from '../../styles/headerLayout.module.css'
const HeaderLayout = () => {
    return (
        <header className={styles.mainHeader}>
            <h3 className={styles.welcome}>Welcome to my personal website</h3>
        </header>
    )
}

export default HeaderLayout;

