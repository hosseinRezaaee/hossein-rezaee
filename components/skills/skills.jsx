import styles from '../../styles/skills.module.css'
import GoToDown from '../goToDown/goToDown'
import Skill from './skill'

const Skills = () => {
    return (
        <>
            <article className={styles.skillsArticle} id="skills">
                <Skill skillName="HTML5" skillProgress="100"/>
                <Skill skillName="CSS3" skillProgress="100"/>
                <Skill skillName="SASS/SCSS" skillProgress="100"/>
                <Skill skillName="BootStrap" skillProgress="80"/>
                <Skill skillName="Flexbox" skillProgress="100"/>
                <Skill skillName="javaScript" skillProgress="90"/>
                <Skill skillName="ES6+" skillProgress="90"/>
                <Skill skillName="Next.js" skillProgress="80"/>
                <Skill skillName="React.js" skillProgress="80"/>
                <Skill skillName="React hooks" skillProgress="90"/>
                <Skill skillName="functional component" skillProgress="80"/>
                <Skill skillName="SSR" skillProgress="80"/>
                <Skill skillName="redux" skillProgress="80"/>
                <Skill skillName="JSX" skillProgress="100"/>
                <Skill skillName="Axios.js" skillProgress="90"/>
                <Skill skillName="REST APIs" skillProgress="100"/>
                <Skill skillName="Material UI" skillProgress="70"/>
                <Skill skillName="Styled Component" skillProgress="90"/>
                <Skill skillName="npm packages" skillProgress="80"/>
                <Skill skillName="jQuery" skillProgress="90"/>
                <Skill skillName="git" skillProgress="80"/>
                <Skill skillName="UI/UX" skillProgress="50"/>
                <Skill skillName="Adobe XD" skillProgress="70"/>
                <Skill skillName="Adobe Photoshop" skillProgress="40"/>
            </article>
            <GoToDown goto={"#projects"} title={"let's head to some of my latest projects"}/>
        </>
    )
}

export default Skills
