import styles from '../../styles/skill.module.css'

const Skill = ({skillName , skillProgress}) => {
    return (
        <div className={styles.skill}>
            <span className={styles.skillName}>{skillName}<span className={styles.doubleDot}> :</span></span>
            <div className={styles.skillProgressWrapper}>
                <div className={styles.skillProgress} style={{width: `${skillProgress}%`}}>
                    <div className={styles.skillProgress2}></div>
                </div>
                <span className={styles.skillPercent}>{skillProgress}%</span>
            </div>
        </div>
    )
}

export default Skill
