import Head from 'next/head';

const HeadLayout = (props) => {
    let prop;
    if (props.children) {
        prop = props.children;
    } else {
        prop =
            (<>
                <title>hossein rezaaee</title>
                <meta name="description" content="front end developer | React.js/Next.js" />
            </>)
    }
    return (
        <Head>
            {prop}
        </Head>
    )
}

export default HeadLayout;
