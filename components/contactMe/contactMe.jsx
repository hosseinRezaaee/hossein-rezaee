import styles from '../../styles/contactMe.module.css'
import GoToDown from '../goToDown/goToDown'
import Contact from './contact'

const ContactMe = () => {
    return (
        <>
            <article className={styles.conactMeArticle} id='contactMe'>
                <div className={styles.contactMeWrapper}>
                    <span className={styles.contactMeText}>Contact me via:</span>
                    <div className={styles.contactMe}>
                        <Contact iconName={'envelope-o'} contactLink={"mailto:hossein.rezaaee99@gmail.com"} />
                        <Contact iconName={'phone'} contactLink={"tel:+989109496900"} />
                        <Contact iconName={'telegram'} contactLink={"https://t.me/Hossein_rezaeezade/"} />
                    </div>
                </div>
                <div className={styles.contactMeWrapper}>
                    <span className={styles.contactMeText}>Follow me on:</span>
                    <div className={styles.contactMe}>
                        <Contact iconName={'linkedin'} contactLink={"https://www.linkedin.com/in/hossein-rezaaee/"} />
                        <Contact iconName={'gitlab'} contactLink={"https://gitlab.com/hosseinRezaaee/"} />
                    </div>
                </div>
                <div className={styles.contactMeWrapper}>
                <span className={styles.contactMeText}>Download my résumé</span>
                    <a href={'/hossein-rezaee resume.pdf'} className={styles.resume} download><i className='fa fa-download'> &nbsp;Download Resume</i></a>
                </div>

            </article>
        </>
    )
}

export default ContactMe
