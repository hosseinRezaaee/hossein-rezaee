import Link from 'next/link'
import styles from '../../styles/contact.module.css'

const Social = ({ contactLink, iconName }) => {
    return (
        <Link href={contactLink}>
            <a>
                <div className={`${styles.contactBox} fa fa-${iconName}`}></div>
            </a>
        </Link>
    )
}

export default Social
