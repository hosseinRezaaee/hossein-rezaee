import styles from '../../styles/intro.module.css'
import GoToDown from '../goToDown/goToDown'

const Intro = () => {
    return (
        <>
            <article className={styles.introArticle}>
                <div className={styles.introText}>
                    <h2 className={styles.headIntro}>My name's Hossein Rezaee</h2>
                    <h3 className={styles.textIntro}>I'm a Front-End web developer</h3>
                    <ul className={styles.descIntro}>
                        <li>
                            <span className={styles.introSpan}>2 years</span> experience at this position
                        </li>
                        <li>
                        Proficiency with <span className={styles.introSpan}>React.js</span>, <span className={styles.introSpan}>Next.js</span> and <span className={styles.introSpan}>jQuery</span>
                        </li>
                        <li>
                        In-depth knowledge of <span className={styles.introSpan}>CSS/SASS</span> and <span className={styles.introSpan}>rsponsive design</span>
                        </li>
                        <li>
                            Studying for a <span className={styles.introSpan}>bachelor's degree</span> in programming
                        </li>
                        <li>
                            <span className={styles.introSpan}>Work experience</span> in companies with different work fields
                        </li>
                        <li>
                            Interested in <span className={styles.introSpan}>learning</span> new technologies and <span className={styles.introSpan}>teamwork</span>
                        </li>
                    </ul>
                </div>
                <div className={styles.introImg}>
                    <div className={styles.imgWrapper}>
                        <img src={"/hosseinRezaee.png"} className={styles.img} alt="hossein rezaee" />
                        {/* <img src={"/hosseinRezaeee.png"} className={styles.img} alt="hossein rezaee" /> */}
                    </div>
                    <div className={styles.thirdSVGWrapper}>
                        <svg className={styles.thirdSVG} viewBox="0 0 90 90" xmlns="http://www.w3.org/2000/svg">
                            <path d="M23.3,-39.2C25.4,-34.9,18.9,-20.3,23.4,-10.7C27.8,-1,43.1,3.7,45.9,8.9C48.7,14.1,39.1,19.8,33.7,33.1C28.3,46.4,27.1,67.2,19.9,70.5C12.8,73.8,-0.3,59.6,-15.3,54.2C-30.4,48.9,-47.5,52.4,-56.2,46.4C-65,40.4,-65.4,24.9,-63.7,11.5C-62.1,-2,-58.4,-13.5,-54.8,-26.5C-51.2,-39.5,-47.6,-53.9,-38.4,-55C-29.3,-56.1,-14.7,-43.7,-2,-40.6C10.6,-37.4,21.2,-43.4,23.3,-39.2Z" />
                        </svg>
                    </div>
                    <div className={styles.fisrtSVGWrapper}>
                        <svg className={styles.fisrtSVG} viewBox="0 0 115 115" xmlns="http://www.w3.org/2000/svg">
                            <path d="M33.5,-59.8C39,-48.6,36,-31.8,34.2,-20.2C32.4,-8.5,31.9,-2,38.1,10.7C44.3,23.3,57.4,42.2,54.5,49.3C51.6,56.4,32.7,51.7,17.9,51.7C3.2,51.7,-7.4,56.5,-19.7,57.3C-32,58.2,-46,55.1,-58.5,47.1C-71,39.1,-81.9,26.2,-82.4,12.8C-82.9,-0.7,-73.1,-14.8,-61.6,-23.3C-50.1,-31.7,-37.1,-34.6,-26.6,-43.5C-16,-52.4,-8,-67.5,3,-72.1C14,-76.8,28,-71,33.5,-59.8Z" />
                        </svg>
                    </div>
                    <div className={styles.secondSVGWrapper}>
                        <svg className={styles.secondSVG} viewBox="0 0 115 115" xmlns="http://www.w3.org/2000/svg">
                            <path d="M39.9,-69.4C50.2,-63.1,56.1,-49.5,61.3,-36.7C66.5,-23.9,70.9,-11.9,66.2,-2.7C61.4,6.5,47.5,12.9,42.6,26.2C37.7,39.4,41.8,59.5,36.2,65.5C30.6,71.5,15.3,63.6,2.4,59.4C-10.5,55.2,-20.9,54.8,-35.2,53.8C-49.6,52.8,-67.7,51.2,-70.4,42.1C-73,33.1,-60.1,16.5,-58,1.3C-55.8,-14,-64.4,-28.1,-62.2,-37.9C-59.9,-47.7,-46.9,-53.2,-34.8,-58.5C-22.6,-63.8,-11.3,-68.9,1.7,-71.8C14.8,-74.8,29.5,-75.8,39.9,-69.4Z" />
                        </svg>
                    </div>
                </div>
            </article>
            <GoToDown goto={'#aboutFront'} title={"Let's learn more about my job "} />
        </>
    )
}

export default Intro;
