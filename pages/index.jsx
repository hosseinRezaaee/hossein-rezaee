import AboutFront from '../components/aboutFront/aboutFront'
import HeadLayout from '../components/head/head-layout'
import Intro from '../components/intro/intro'
import MainLayout from '../components/layouts/mainLayout'
import Skills from '../components/skills/skills'
import WorkExperience from '../components/workExperience/workExperience'
import {useSelector} from 'react-redux'
import store from '../redux/store'
import ContactMe from '../components/contactMe/contactMe'

export default function Home() {
    const bgColor = useSelector(store => store.bgState)
    // let ref = "ref"
    return (   
        <div style={{backgroundColor: bgColor}}>            
            <HeadLayout/>  
            <MainLayout>
                <Intro/>
                <AboutFront/>
                <Skills/>
                <WorkExperience/>
                <ContactMe/>
            </MainLayout>
        </div>
    )
}