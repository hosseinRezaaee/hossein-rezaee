export const bgReducer = function(state = '', action) {
    switch (action.type) {
        case 'FEENAB':
            return '#00A79D';
        case 'NOUYAN':
            return '#7DC148';
        case 'MY_WEBSITE':
            return '#D63A5F';
        default:
            return '#333'
    }
}