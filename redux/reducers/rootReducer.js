import { combineReducers } from 'redux'
import { bgReducer } from './bgReducer'

const reducers = combineReducers({
    bgState: bgReducer
})
export default reducers;